---
categories: ["Konzepte"]
tags: ["twig", "api"]
title: "Konzept Beispiel"
linkTitle: "Konzept Beispiel"
date: 2017-01-05
description: >
  Das hier könnte eine **Konzept** Beispiel Seite sein
---

**updated**
{{% pageinfo %}}
**Achtung** Konzept ist noch nicht fertig :)

und warum PHP code formatierung nicht funktioniert, weiss nicht nicht... aber das Diagram ist cool
{{% /pageinfo %}}

## Ein mermaid Diagram

```mermaid
graph LR
  Start --> Need{"Do I need diagrams"}
  Need -- No --> Off["Set params.mermaid.enable = false"]
  Need -- Yes --> HaveFun["Great!  Enjoy!"]
```

```go
func main() {
  input := `var foo = "bar";`

  lexer := lexers.Get("javascript")
  iterator, _ := lexer.Tokenise(nil, input)
  style := styles.Get("github")
  formatter := html.New(html.WithLineNumbers())

  var buff bytes.Buffer
  formatter.Format(&buff, style, iterator)

  fmt.Println(buff.String())
}
```
jo

```php
echo "hallo welti";
```

```
<?php
echo "hallo welti";

public function(array $test): array {
  echo "hallo";
}
```


Text can be **bold**, _italic_, or ~~strikethrough~~. [Links](https://gohugo.io) should be blue with no underlines (unless hovered over).
